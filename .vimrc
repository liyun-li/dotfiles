syntax on
filetype on
colorscheme desert

set autoread
set ts=2 sw=2 expandtab si ci " indentation
set nu " line number
set ignorecase smartcase " case insensitive search

" Color scheme settings
set pastetoggle=<F2> " paste properly from the system clipboard

" unset the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>

