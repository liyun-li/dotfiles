#!/bin/bash

# install git and openssh on the windows host
# enable ssh on keepassxc

for bin in /mnt/c/Windows/System32/OpenSSH/*.exe; do
  symlink=`echo $bin | sed 's/.*\///' | sed 's/\.exe//'`
  sudo ln -s $bin /usr/local/bin/$symlink
done

for bin in '/mnt/c/Program Files/Git/cmd/'*.exe; do
  symlink=`echo $bin | sed 's/.*\///' | sed 's/\.exe//'`
  sudo ln -s $bin /usr/local/bin/$symlink
done
