# Dependency
## Do not install Git or SSH if using WSL. Create symlinks instead (e.g. ln -s "`which git.exe` /usr/local/bin/git")
## the fcitx5 packages are just examples
## pacman -Sy git curl fzf python-pip python-pipx python-virtualenv python-virtualenvwrapper zsh-autosuggestions zsh-syntax-highlighting man-db nodejs npm kwalletmanager packagekit-qt6 power-profiles-daemon wl-clipboard fcitx5 firefox discord signal-desktop keepassxc powertop noto-fonts-cjk noto-fonts-emoji fcitx5-im fcitx5-chinese-addons fcitx5-skk
#git config --global user.name "Liyun"
#git config --global user.email "7397775-liyun-li@users.noreply.gitlab.com"

export MANPAGER="less -R --use-color -Dd+r -Du+b"
export FLYCTL_INSTALL="${HOME}/.fly"
export WORKON_HOME="${HOME}/.virtualenvs"
export PATH="${FLYCTL_INSTALL}/bin:${HOME}/.local/bin:${PATH}"

PROMPT_NEWLINE=$'\n'
PROMPT="%(?.%F{green}[0.%F{red}[%?)] %n@%m %~${PROMPT_NEWLINE}%F{yellow}$%b%f "
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000

unsetopt beep
setopt appendhistory extendedglob nomatch globdots share_history
set bell-style none
bindkey -e

zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'

autoload -Uz compinit colors
compinit
colors

alias c='clear'
alias ls='ls -CF --color'
alias ll='ls -l'
alias la='ls -a'
alias l='ls'
alias grep='grep --color'
alias x='xclip -sel clip'

# WSL detection
if [[ -f /proc/sys/fs/binfmt_misc/WSLInterop || -f $(which powershell.exe) ]]; then
  # Useful when you use an SSH agent on the Windows host
  alias e='explorer.exe'
else
  alias e='xdg-open'
fi

cmd_alias='..'
cmd_actual='cd ..'
for i in {1..10}; do
  alias "${cmd_alias}"="${cmd_actual}"
  cmd_alias="${cmd_alias}."
  cmd_actual="${cmd_actual}/.."
done

bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
bindkey '\e[5~' beginning-of-history
bindkey '\e[6~' end-of-history
bindkey '\e[7~' beginning-of-line
bindkey '\e[3~' delete-char
bindkey '\e[2~' quoted-insert
bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word
bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line
bindkey '^[[3~' delete-char
bindkey '^H' backward-kill-word

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
source /usr/bin/virtualenvwrapper.sh

# A few more useful Git commands for WSL
#git config --global core.editor "vim"
#git config --global --add safe.directory '*'
#git config --global core.autocrlf false

alias f='flyctl'
